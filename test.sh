#!/bin/bash

set -e

rm -rf test/ golden/
mkdir -p test/ golden/
cp Makefile test/
tar -x -C test/ -f golden.tgz
tar -x -C golden/ -f golden.tgz

make -C test

echo " ======== Golden ========="
find golden/ -name "cat*" -exec file {} \;
echo " ======== Test ========="
find test/ -name "cat*" -exec file {} \;
