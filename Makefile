FILES := cat cat.sh

prefix := /foo
INSTALLED_FILES:= $(patsubst %,$(prefix)/%,$(FILES))

all: $(INSTALLED_FILES)

$(INSTALLED_FILES): $(prefix)/% : % 
	@echo $@
